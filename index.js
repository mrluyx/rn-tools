/** @format */

// @flow

import { Linking } from "react-native";

type DirectType = "whatsapp" | "tel" | "mailto" | "sms";

const getUrlFromDirectType = (directType?: ?DirectType, string: string) => {
  if (directType === "whatsapp") {
    return `${directType}://send?text=&phone=${string}`;
  }

  if (directType === "tel" || directType === "mailto" || directType === "sms") {
    return `${directType}:${string}`;
  }

  return string;
};

export const stopenUrl = async (
  directType?: ?DirectType,
  string: string
): boolean => {
  if (!string) return false;

  const url = getUrlFromDirectType(directType, string);
  const supported = await Linking.canOpenURL(url);

  if (supported) {
    try {
      await Linking.openURL(url);
    } catch (error) {
      return false;
    }

    return true;
  }

  return false;
};
